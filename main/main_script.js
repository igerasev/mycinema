document.addEventListener('DOMContentLoaded', function () {
    filmArray = localStorage.getItem("films");
    if (filmArray === null) {
        getAllFilmsFromServer();
    } else {
        filmArray = JSON.parse(filmArray);
        makeFilters();
        printFilmsFromArray();

    }
});

let fastSearchInput = document.querySelector(".search");
fastSearchInput.addEventListener("input", fastSearch);

let typeSort = document.querySelector(".filmType");
typeSort.addEventListener("change", sortByType);

let yearSort = document.querySelector(".filmYear");
yearSort.addEventListener("change", sortByYear);

window.onpopstate = function (event) {
    // debugger
    console.log(event.target);
    if (document.location.hash == '') {
        console.log(location.pathname);
        clearAndPrint();
    }
};
//
// window.addEventListener("dragend",(event)=>{
//     let el = event.target;
//     switch (el.className.indexOf("filmline__element")!=-1) {
//         case true:
//             dragMouseDown();
//             break;
//
//     }
// });
//
// window.addEventListener("mouseup",(event)=>{
//     let el = event.target;
//     switch (el.className) {
//         case "filmline__element":
//             dragMouseUp();
//             break;
//
//     }
// });


window.addEventListener("click", (event) => {
    let el = event.target;
    switch (el.className) {
        case "buttons__edit":
            editFilm(event);
            break;
        case "buttons__del":
            deleteFilm(event);
            break;
        case "sendfBtn":
            sendFilm(event);
            break;
        case "changefBtn":
            saveChanges(event.target.getAttribute("fId"));
            break;
        case "addFilm":
            addFilm(event);
            break;
        case "search":
            clearSearch();
            break;
        case "addEdit__line addEdit__year":
            el.textContent="";
            break;
        case "addEdit__line addEdit__name":
            el.textContent="";
            break;
        case "addEdit__line addEdit__type":
            el.textContent="";
            break;

    }
});

function dragMouseDown() {
    alert(event.target);

}

function dropElement(){
    let dragId = event.target.getAttribute("id");
    let staticId = +document.elementFromPoint(getCursorPosition(event).x,getCursorPosition(event).y).getAttribute("fId");
    let i = +dragId;
    while(i!=staticId) {
        if (i>staticId) {
            i--;
            if(filmArray[i].id!=""+filmArray.length) filmArray[i].id=(Number.parseInt(filmArray[i].id)+1).toString();
        }
        else if (i<staticId) {
            i++;
            if(filmArray[i].id!="0") filmArray[i].id=(Number.parseInt(filmArray[i].id)-1).toString();
        }

    }

    filmArray[dragId].id=""+staticId;
    filmArray.sort(function(a, b) {
        if (+a.id > +b.id) return 1;
        if (+a.id == +b.id) return 0;
        if (+a.id < +b.id) return -1;
    });
    localStorage.setItem("films", JSON.stringify(filmArray));
    clearAndPrint();
}

function makeFilters() {
    let parentYear = document.querySelector(".filmYear");
    let parentType = document.querySelector(".filmType");
    let localeStorageYear = localStorage.getItem("yearFilter");
    let localStorageType = localStorage.getItem("typeFilter");
    let localFastSearch = localStorage.getItem("fastSearch");
    localeStorageYear ? yearFilter = localeStorageYear : "";
    localStorageType ? typeFilter = localStorageType : "";
    let allyears = new Set(), alltypes = new Set();
    localFastSearch ? fastSearchFilter = localFastSearch :"";
    fastSearchFilter ? document.querySelector(".search").value=fastSearchFilter : "Быстрый поиск";
    filmArray.forEach((item) => {
        if (!allyears.has(item.year)) {
            let yearOption = document.createElement("option");
            if (yearFilter==item.year) yearOption.selected=true;
            yearOption.textContent = item.year;
            yearOption.value = item.year;
            parentYear.append(yearOption);
        }

        if (!alltypes.has(item.type)) {
            let typeOption = document.createElement("option");
            if (typeFilter==item.type) typeOption.selected=true;
            typeOption.textContent = item.type;
            typeOption.value = item.type;
            parentType.append(typeOption);
        }

        allyears.add(item.year);
        alltypes.add(item.type);
    });
}

function sortByType() {
    typeFilter = this.value;
    localStorage.setItem("typeFilter", typeFilter);
    clearAndPrint();
}

function sortByYear() {
    yearFilter = this.value;
    localStorage.setItem("yearFilter", yearFilter);
    clearAndPrint();
}


function clearSearch() {
    localStorage.setItem("fastSearch","");
    event.target.value = "";


}

function fastSearch(event) {
    fastSearchFilter = event.target.value.toLowerCase();
    localStorage.setItem("fastSearch",fastSearchFilter);
    clearAndPrint();
}

function hidePopup() {
    let hint = document.querySelector(".popup");
    hint.style.display = "none";
    // document.querySelector(".popup").display="none";
}

function movePopup() {
    let hint = document.querySelector(".popup");
    hint.style.left = (getCursorPosition(event).x+20) + "px";
    hint.style.top = (getCursorPosition(event).y+20) + "px";


}

function showPopup(event) {
    let id = event.target.getAttribute("fId");
    let hint = document.querySelector(".popup");

    document.querySelector(".popup__name").textContent = filmArray[id].filmName;
    document.querySelector(".popup__year").textContent = filmArray[id].year;
    hint.style.left = (getCursorPosition(event).x+20) + "px";
    hint.style.top = (getCursorPosition(event).y+20) + "px";
    hint.style.height = "100px;";
    hint.style.width = "100px";
    hint.style.display = "block";
    hint.style.zIndex = "2000";

    // console.log(event);
}

function getCursorPosition(e) {
    let x, y = 0;

    if (e.pageX || e.pageY) {
        x = e.pageX;
        y = e.pageY;
    } else if (e.clientX || e.clientY) {
        x = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
        y = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
    }

    return {x: x, y: y}
}

function clearAndPrint() {
    let main_wrapper = document.querySelector(".main__wrapper");
    main_wrapper.innerHTML = "";
    main_wrapper.style.display = "block";
    document.querySelector(".main__addEdit").style.display = "none";
    document.querySelector("footer").style.display = "block";
    document.querySelector(".header__wrapper").style.display = "flex";
    document.querySelector(".popup").style.display = "none";
    printFilmsFromArray();
}

function getAllFilmsFromServer() {
    function loadJson(callback) {
        let XmlHttpRequest = createRequestObject();
        XmlHttpRequest.overrideMimeType("application/json");
        XmlHttpRequest.open('GET', 'db/films.json', true);
        XmlHttpRequest.onreadystatechange = function () {
            if (XmlHttpRequest.readyState == 4 && XmlHttpRequest.status == "200") {
                callback(XmlHttpRequest.responseText);
            }
        }
        XmlHttpRequest.send(null);
    }

    loadJson(function (response) {
        filmArray = JSON.parse(response).films;
        clearAndPrint();
        localStorage.setItem("films", JSON.stringify(filmArray));
    });
}

/*
<div class="main__wrapper__filmline">
    <div class="filmline__element">
        <div class="filmline__element__cover">1</div>
        <div class="filmline__element__name">name</div>
        <div class="filmline__element__buttons">
            <div class="buttons__edit"></div>
            <div class="buttons__del"></div>
        </div>
    </div>
</div>*/
function deleteFilm(event) {
    let id = event.target.getAttribute("fId");
    let fElement = document.getElementById(id);
    if (confirm("Вы действительно хотите удалить " +
        fElement.querySelector(".filmline__element__name").textContent)) {
        sendDeleteReq(id, fElement);
    }
}

function sendDeleteReq(id, fElement) {
    var http = createRequestObject();
    if (http) {
        http.open('get', "ServerResponse_deletefilm.json");
        http.onreadystatechange = function () {
            if (http.readyState == 4) {
                function okey() {
                    let resp = JSON.parse(http.responseText);
                    if (resp && resp.deletefilm) {
                        fElement.remove();
                        filmArray.splice(id, 1);
                        let a = filmArray;
                        localStorage.setItem("films", JSON.stringify(filmArray));
                        alert("Фильм успешно удален");
                        clearAndPrint();
                    } else alert("Ошибка удаления");
                }

                okey();
            }
        }
        http.send(null);
    }
}

function showContent(link) {
    var http = createRequestObject();
    if (http) {
        http.open('get', link);
        http.onreadystatechange = function () {
            if (http.readyState == 4) {
                document.write(http.responseText);
            }
        }
        http.send(null);
    } else {
        document.location = link;
    }
}

// создание ajax объекта
function createRequestObject() {
    try {
        return new XMLHttpRequest()
    } catch (e) {
        try {
            return new ActiveXObject('Msxml2.XMLHTTP')
        } catch (e) {
            try {
                return new ActiveXObject('Microsoft.XMLHTTP')
            } catch (e) {
                return null;
            }
        }
    }
}

function displayAddEditBlock(film, id) {
    document.querySelector("footer").style.display = "none";
    document.querySelector(".main__wrapper").style.display = "none";
    document.querySelector(".header__wrapper").style.display = "none";


    let block1 = document.querySelector(".main__addEdit");
    block1.innerHTML = "";
    block1.style.display = "grid";

    let submit_send = document.createElement("div");
    submit_send.className = "addEdit__submit__send";
    submit_send.style.display = "none";


    let input_sendf = document.createElement("input");
    input_sendf.className = "sendfBtn";
    input_sendf.setAttribute("type", "submit");
    input_sendf.setAttribute("id", "submit");
    input_sendf.setAttribute("fId", id);
    input_sendf.setAttribute("value", "Загрузить");
    // input_sendf.addEventListener(onclick, sendFilm);


    let addEdit_file = document.createElement("div");
    addEdit_file.className = "addEdit__file";
    addEdit_file.style.display = "none";

    let input_file = document.createElement("input");
    input_file.setAttribute("type", "file");
    input_file.setAttribute("id", "film");

    addEdit_file.append(input_file);

    submit_send.append(input_sendf);

    let submit_change = document.createElement("div");
    submit_change.className = "addEdit__submit__change";
    submit_change.style.display = "block";

    let input_changef = document.createElement("input");
    input_changef.className = "changefBtn";
    input_changef.setAttribute("type", "submit");
    input_changef.setAttribute("id", "change");
    input_changef.setAttribute("value", "Изменить");
    input_changef.setAttribute("fId", id);
    // input_changef.addEventListener(onclick, saveChanges);
    submit_change.append(input_changef);


    //         <div class="addEdit__submit__send"><input id="submit" type="submit" value="Загрузить" onclick="sendFilm()"/></div>
//         <div class="addEdit__submit__change"><input id="change" type="submit" value="Изменить" onclick="saveChanges(history.state)"/></div>
//         <div class="addEdit__back"><a href="/mycinema/index.html">Назад</a></div>

    if (film.action_add == true) {//edit
        film = {
            cover: "db/upload.jpeg",
            filmName: "Введите название фильма",
            type: "Введите жанр фильма",
            year: "Введите год выхода",
            action_add: false,
        };
        addEdit_file.style.display = "block";
        // document.querySelector(".addEdit__file").style.display = "block";
        submit_send.style.display = "block";
        submit_change.style.display = "none";
    }

    let fElement_cover = document.createElement('div');
    fElement_cover.className = "filmline__element__cover addEdit__line";
    fElement_cover.style.background = `url('${film.cover}') no-repeat center center`;
    block1.append(fElement_cover);
    let nameEdit = document.createElement("div");
    nameEdit.className = "addEdit__line addEdit__name";
    nameEdit.setAttribute("contenteditable", "true");
    let typeEdit = document.createElement("div");
    typeEdit.className = "addEdit__line addEdit__type";
    typeEdit.setAttribute("contenteditable", "true");
    let yearEdit = document.createElement("div");
    yearEdit.setAttribute("contenteditable", "true");
    yearEdit.className = "addEdit__line addEdit__year";

    nameEdit.textContent = film.filmName;
    yearEdit.textContent = film.year;
    typeEdit.textContent = film.type;

    block1.append(addEdit_file, nameEdit, typeEdit, yearEdit, submit_change, submit_send);
}

function sendFilm() {
    document.getElementById("film").style.getPropertyValue("display");
    if (document.getElementById("film").files.length > 0) {
        sendToServer();
        let newFilm = {
            "filmName": document.querySelector(".addEdit__name").textContent,
            "year": document.querySelector(".addEdit__year").textContent,
            "type": document.querySelector(".addEdit__type").textContent,
            "cover": "/mycinema/db/film.jpeg"
        };
        filmArray.push(newFilm);
        localStorage.setItem("films", JSON.stringify(filmArray));

    }else alert("Выберите файл");
}

function sendToServer() {
    var http = createRequestObject();
    if (http) {
        http.open('get', "ServerResponse_addfilm.json");
        http.onreadystatechange = function () {
            if (http.readyState == 4) {
                function okey() {
                    let resp = JSON.parse(http.responseText);
                    if (resp && resp.addfilm) {
                        alert("Фильм успешно загружен");
                    } else alert("Ошибка загрузки");
                }

                okey();
            }
        }
        http.send(null);
    }
}

function editFilm() {
    let id = event.target.getAttribute("fId");
    document.location.hash += "#edit";
    displayAddEditBlock(filmArray[id], id);
    // showContent("edit.html");
}

function saveChanges(id) {
    filmArray[id].filmName = document.querySelector(".addEdit__name").textContent;
    filmArray[id].year = document.querySelector(".addEdit__year").textContent;
    filmArray[id].type = document.querySelector(".addEdit__type").textContent;
    localStorage.setItem("films", JSON.stringify(filmArray));
    alert("Изменено успешно!");
}

function addFilm() {
    history.pushState(null, "Edit", "edit");
    // showContent("edit.html");
    displayAddEditBlock({action_add: true});
}

function printFilmsFromArray() {
    let fSearch = fastSearchFilter;
    let tempYearFilter, tempTypeFilter;
    let count = 0; //max 5
    let mainWrapper = document.querySelector(".main__wrapper");
    mainWrapper.style.display = "grid";
    let parentDiv = document.createElement('div');
    parentDiv.className = 'main__wrapper__filmline';
    for (let i = 0; i < filmArray.length; i++) {

        let item = filmArray[i];
        let fId = filmArray[i].id;
        if (typeof(fId)!="string"){
            filmArray[i].id=""+i;
            fId=""+i;
        }
        if (i == filmArray.length - 1) {
            mainWrapper.append(parentDiv);
        }
        typeof (typeFilter) == "undefined" || typeFilter == "" ? tempTypeFilter = filmArray[i].type : tempTypeFilter = typeFilter;
        typeof (yearFilter) == "undefined" || yearFilter == "" ? tempYearFilter = filmArray[i].year : tempYearFilter = yearFilter;

        if (filmArray[i].type == tempTypeFilter && filmArray[i].year == tempYearFilter) {
            let values = Object.values(item);
            values.every((val_item) => {


                if (typeof (fSearch) == "undefined" || fSearch == "" || typeof(val_item)!="boolean" && val_item.indexOf(fSearch) != -1) {


                    let fElement = document.createElement('div');
                    fElement.className = "filmline__element";
                    fElement.setAttribute("id", fId);
                    fElement.setAttribute("draggable","true");
                    fElement.style.zIndex="1000";
                    // fElement.style.order=fId;
                    fElement.addEventListener("dragend",dropElement);
                    let fElement_cover = document.createElement('div');
                    fElement_cover.className = "filmline__element__cover";
                    fElement_cover.setAttribute("fId", fId);
                    fElement_cover.addEventListener("mouseover", showPopup);
                    fElement_cover.addEventListener("mouseout", hidePopup);
                    fElement_cover.addEventListener("mousemove", movePopup);
                    fElement_cover.style.background = `url('${item.cover}') no-repeat center center`;
                    fElement.append(fElement_cover);
                    let fElement_name = document.createElement("div");
                    fElement_name.className = "filmline__element__name";
                    fElement_name.textContent = item.filmName;
                    fElement.append(fElement_name);
                    let fElement_type = document.createElement("div");
                    fElement_type.className = 'filmline__element__year';
                    fElement_type.textContent = item.type;
                    fElement.append(fElement_type);
                    let fElement_buttons = document.createElement('div');
                    fElement_buttons.className = "filmline__element__buttons";
                    let fElement_btnEdit = document.createElement('div');
                    // fElement_btnEdit.onclick = editFilm;
                    fElement_btnEdit.setAttribute("fId", fId);
                    fElement_btnEdit.className = "buttons__edit";
                    let fElement_year = document.createElement('div');
                    fElement_year.className = 'filmline__element__year';
                    fElement_year.textContent = item.year;
                    let fElement_btnDel = document.createElement('div');
                    fElement_btnDel.className = "buttons__del";
                    fElement_btnDel.setAttribute("fId", fId);
                    // fElement_btnDel.onclick = deleteFilm;
                    fElement_buttons.append(fElement_btnEdit);
                    fElement_buttons.append(fElement_year);
                    fElement_buttons.append(fElement_btnDel);
                    fElement.append(fElement_buttons);
                    if (count == 5) {
                        mainWrapper.append(parentDiv);
                        parentDiv = document.createElement('div');
                        parentDiv.className = "main__wrapper__filmline";
                        count = 0;
                    } else {
                        count++;
                    }
                    parentDiv.append(fElement);
                    if (i == filmArray.length - 1) {
                        mainWrapper.append(parentDiv);
                    }

                    return false;
                }

                return true;

            });
        }


    }

}
